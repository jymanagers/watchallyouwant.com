import os
import feedparser
import pickle

from flask import Flask, request, render_template, url_for, redirect
# from flask_sqlalchemy import SQLAlchemy
# from flask_migrate import Migrate
# from forms import ContactForm, AdoptUploadForm

from config import Config
# from FAPManager import FAPManager
# from models import *

app = Flask(__name__, static_url_path='/static')
app.config.from_object(Config)
# db = SQLAlchemy(app)
# migrate = Migrate(app, db)
# logger = FAPManager()


@app.route('/')
def home():
    return render_template('pages/home.html')

@app.route('/search', methods=['GET','POST'])
def search():
    if request.method == 'POST':
        referer = request.referrer
        if 'asian' in referer:
            region = 'asian'
        elif 'western' in referer:
            region = 'western'
        else:
            region = None
        return redirect((url_for('search', query=request.form.get('search'),region=region)))
    
    region = request.args.get('region')
    query = request.args.get('query')

    if query == None:
        return render_template('pages/error.html')

    data = []
    if region == 'asian':
        with open('static/data/asian.pkl','rb') as f:
            data.extend(pickle.load(f))
    elif region == 'western':
        with open('static/data/western.pkl','rb') as f:
            data.extend(pickle.load(f))
    else:
        with open('static/data/western.pkl','rb') as f:
            data.extend(pickle.load(f))
        with open('static/data/asian.pkl','rb') as f:
            data.extend(pickle.load(f))
    
    results = []
    for item in data:
        if query.lower() in item['title'].lower():
            results.append(item)

    if len(results) == 0:
        query = 'No Results'
    else:
        query = 'Results of "' + query + '"'
    data = None
    return render_template('pages/main.html', data=sort_data(results), heading=query, previous_page=None, current_page=None, next_page=None)


@app.route('/asian/')
def asian():

    data = asian_parse(app.config['ASIAN_URL'] + 'drama')[0]
    data = asian_clean_url(data)

    return render_template('pages/textual.html', data=data[:-1], heading='Asian')


@app.route("/asian/<path:relative_url>")
def asian_rss(relative_url):
    image = request.args.get('img')
    if (relative_url == 'drama') or relative_url == "drama/":
        return render_template('pages/error.html')

    data, title, pages = asian_parse(app.config['ASIAN_URL'] + relative_url)
    if (data == None):
        return render_template('pages/error.html')
    data = asian_clean_url(data)

    if (pages == None):
        previous_page, current_page, next_page = None, None, None

    else:
        if (len(pages) == 1):
            tmp = pages[0]
            if 'back' in tmp['image']:
                previous_page, current_page, next_page = tmp, tmp['page'], None
            else:
                previous_page, current_page, next_page = None, 1, tmp

        else:
            previous_page = min(pages, key=lambda x: x['page'])
            next_page = max(pages, key=lambda x: x['page'])
            current_page = int((previous_page['page'] + next_page['page'])/2)

    if 'play3' not in data[0]['image']:
        template = "pages/main.html"
        for item in data:
            item['link'] = item['link'] + '?img=' + item['image']

    elif 'info' in relative_url:
        template = "pages/episode.html"
        if image != None:
            if previous_page != None:
                previous_page['link'] = previous_page['link'] + '?img=' + image
            if next_page != None:
                next_page['link'] = next_page['link'] + '?img=' + image

    else:
        template = "pages/textual.html"

    return render_template(template, data=sort_data(data), heading=title, previous_page=previous_page, current_page=current_page, next_page=next_page, image=image)


def asian_parse(link):

    d = feedparser.parse(link)
    if len(d) == 0:
        return None, None, None

    entries = d.entries
    try:
        details = [{"title": entry["title"], "image":entry["summary"].replace("'", '"').split(
            '"')[1].strip(), "link":entry["links"][0]["href"]} for entry in entries]
    except Exception as e:
        return None, None, None

    pages = [{'page': int(item['title'].strip().split()[-1]), 'link':item['link'], 'image':item['image']}
             for item in details[-2:] if item['title'].lower().startswith('page')]

    if len(pages) == 0:
        pages = None
    else:
        details = details[:-len(pages)]
        pages = asian_clean_url(pages)

    if len(details) == 0:
        return None, None, None
    return details, d.feed["title"], pages


def asian_clean_url(data):

    for item in data:
        item["link"] = item['link'].replace(app.config['ASIAN_URL'], url_for('asian'))
    return data


@app.route('/western/')
def western():
    region = "Western"
    data = [
        {
            'link': 'http://us.vrss.me/ustvraw.php?channel=hot',
            'title': 'HOT'
        },
        {
            'link': 'http://us.vrss.me/ustvraw.php?channel=just-updated',
            'title': 'Recently Added'
        },
        {
            'link': 'http://us.vrss.me/ustvraw.php?channel=movies',
            'title': 'Movies'
        },
        {
            'link': 'http://us.vrss.me/ustvraw.php?channel=tv-series',
            'title': 'TV Series'
        }
    ]
    data = western_clean_url(data)
    return render_template('pages/textual.html', data=data, heading=region)


@app.route("/western/shows")
def western_rss():

    image = request.args.get('img')
    current_page = request.args.get('page')
    query = request.query_string.decode()

    data, title, next_page = western_parse(app.config['WESTERN_URL'] + query)

    if (data == None):
        return render_template('pages/error.html')

    if 'channel' in query:
        data = western_clean_url(data)
        template = "pages/main.html"
    else:
        for item in data:
            item['link'] = item['link'].split('?', 1)[0]
        template = "pages/episode.html"

    if current_page == None:
        current_page = 1
        previous_page = None
    else:
        current_page = int(current_page)
        previous_page = {
            'link': '/western',  # to be fixed
            'page': current_page - 1
        }

    return render_template(template, data=data, heading=title, previous_page=previous_page, current_page=current_page, next_page=next_page, image=image)


def western_clean_url(data):

    for item in data:
        item["link"] = url_for('western_rss') + '?' + item["link"].split('?', 1)[-1]
    return data


def western_parse(link):

    d = feedparser.parse(link)
    if len(d) == 0:
        return None, None, None

    entries = d.entries
    try:
        if 'channel' not in link:
            details = [{"title": entry["title"], "link":entry["links"]
                        [0]["href"]} for entry in entries if entry['title']]
        else:
            details = [{"title": entry["title"], "image":entry["summary"].replace("'", '"').split(
                '"')[1].strip(), "link":entry["links"][0]["href"]} for entry in entries if entry['title']]
    except Exception as e:
        return None, None, None

    tmp = details[-1]
    if tmp['title'].lower().startswith('page'):
        next_page = {
            'page': int(tmp['title'].strip().split()[1]),
            'link': url_for('western_rss') + '?' + tmp['link'].split('?', 1)[-1]
        }
        details = details[:-1]
    else:
        next_page = None
    tmp = None

    if len(details) == 0:
        return None, None, None
    return details, d.feed["title"], next_page


def sort_data(data):
    return sorted(data, key=lambda i: i['title'])


@app.route("/about")
def about():
    return render_template("pages/about.html")

@app.errorhandler(404)
def invalid_route(e):
    return render_template('pages/error.html', error="Page Not Found", statuscode="404")

if __name__ == '__main__':
    app.run(debug=True)
