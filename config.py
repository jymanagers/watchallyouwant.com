import os

class Config(object):
    # set environment variable when in
    SECRET_KEY = os.environ.get('SECRET_KEY') or '.?[R36CQ+/jbu=<p#:5M'
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL')
    ASIAN_URL = "http://myrss.nu/"
    WESTERN_URL = "http://us.vrss.me/ustvraw.php?"
